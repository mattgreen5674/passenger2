<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\PostCode;

class PostCodeController extends Controller
{
    public function index(array $data = [])
    {
        return view('postcode', ['data' => $data]);
    }

    public function latLongForm(Request $request)
    {
        try {
            $validated = $request->validate(PostCode::getRules('lat_long'));

            $conditions = [];
            
            if (!empty($validated['latitude'])) {
                $conditions[] = ['latitude', 'LIKE', "{$validated['latitude']}%"];
            }
            
            if (!empty($validated['longitude'])) {
                $conditions[] = ['longitude', 'LIKE', "{$validated['longitude']}%"];
            }

            $data = PostCode::select('id', 'post_code', 'latitude', 'longitude')
                ->where($conditions)
                ->limit(30) // @todo add pagination for larger results
                ->get();
            
            $formattedData = PostCode::tabulariseData($data->toArray(), 'latLongSearchResults');

            return response()->json($formattedData);

        } catch (\Illuminate\Database\QueryException $e) {

            return response()->json(['success' => false, 'message' => $e->getMessage()]);
        } catch (Exception $e) {
        
            return response()->json(['success' => false, 'message' => $e->getMessage()]);
        }
    }

    public function postCodeForm(Request $request)
    {
        try {
            $validated = $request->validate(PostCode::getRules('post_code'));
            $searchable = strtolower(str_replace(' ', '', $validated['post_code']));
            
            $data = PostCode::select('id', 'post_code', 'latitude', 'longitude')
                ->where('searchable', 'LIKE', "%$searchable%")
                ->limit(30) // @todo add pagination for larger results 
                ->get();
            
            $formattedData = PostCode::tabulariseData($data->toArray(), 'postCodeSearchResults');

            return response()->json($formattedData);
        
        } catch (\Illuminate\Database\QueryException $e) {

            return response()->json(['success' => false, 'message' => $e->getMessage()]);
        } catch (Exception $e) {
        
            return response()->json(['success' => false, 'message' => $e->getMessage()]);
        }
    }
}
