<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostCode extends Model
{
    use HasFactory;
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    const DEFAULT_TYPE = 'default';
    const LAT_LONG_FORM_TYPE = 'lat_long';
    const POST_CODE_FORM_TYPE = 'post_code';
    
    private static $importRules = [
        'post_code' => 'required|min:7|max:9|regex:"[a-zA-Z0-9\s]+"',
        'latitude' => 'required|numeric|min:-90|max:90',
        'longitude' => 'required|numeric|min:-180|max:180',
    ];

    private static $postCodeRules = [
        'post_code' => 'required|max:9|regex:"[a-zA-Z0-9\s]+"',
    ];

    private static $latLongRules = [
        'latitude' => 'nullable|numeric|min:-90|max:90',
        'longitude' => 'nullable|numeric|min:-180|max:180',
    ];



    public static function getRules(string $type = self::DEFAULT_TYPE) : array
    {
        if ($type === self::DEFAULT_TYPE) {
            return self::$importRules;
        }

        if ($type === self::LAT_LONG_FORM_TYPE) {
            return self::$latLongRules;
        }

        if ($type === self::POST_CODE_FORM_TYPE) {
            return self::$postCodeRules;
        }
    }

    public static function tabulariseData(array $data, string $idName) : array
    {
        if (empty($data)) {
            return [
                'success' => true, 
                'result' => "<tbody id='$idName'><tr><td align='centre' colspan='4'>There are no post codes to display</td></tr></tbody>"
            ];
        }

        $formatted_data = "<tbody id='$idName'>";

        foreach($data as $data_item){
            $formatted_data .= '<tr>';

            foreach ($data_item as $item) {
                $formatted_data .= "<td>$item</td>";
            }
            $formatted_data .= '</tr>';
        }

        $formatted_data .= '</tbody>';

        return ['success' => true, 'result' => $formatted_data];
    }
}
