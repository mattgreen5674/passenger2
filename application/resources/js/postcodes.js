$(document).ready(function(){
    
    $('#postCodeForm').submit(function( event ) {
        event.preventDefault();
        
        axios.post('/postcodeform', {
            post_code: $('#postcode').val(),
        })
        .then(function (response) {
            if (response.data.success) {
                $('#postCodeSearchResults').replaceWith(response.data.result);
            } else {
                $('#postCodeFormError').slideDown().delay(5000).slideUp();
            }
        })
        .catch(function (error) {
            $('#postCodeErrorList').append(`<p>${error.response.data.message}</p>`);
            
            error.response.data.errors.post_code.forEach(function(error, index){
                $('#postCodeErrorList').append(`<p>${error}</p>`);
            });
            
            $('#postCodeFormError').slideDown().delay(10000).slideUp();
        });
    });

    $('#latLongForm').submit(function( event ) {
        event.preventDefault();
        
        axios.post('/latlongform', {
            latitude: $('#latitude').val(),
            longitude: $('#longitude').val(),
        })
        .then(function (response) {
            if (response.data.success) {
                $('#latLongSearchResults').replaceWith(response.data.result);
            } else {
                $('#latLongFormError').slideDown().delay(10000).slideUp();
            }   
        })
        .catch(function (error) {
            $('#latLongErrorList').append(`<p>${error.response.data.message}</p>`);
            
            if('latitude' in error.response.data.errors){
                error.response.data.errors.latitude.forEach(function(error, index){
                    $('#latLongErrorList').append(`<p>${error}</p>`);
                });
            }

            if('longitude' in error.response.data.errors){
                error.response.data.errors.longitude.forEach(function(error, index){
                    $('#latLongErrorList').append(`<p>${error}</p>`);
                });
            }

            $('#latLongFormError').slideDown().delay(5000).slideUp();
        });
    });
})