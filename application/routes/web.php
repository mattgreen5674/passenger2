<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostCodeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
//});

//Route::get('/', [HomeController::class, 'index']); 
Route::get('/', [PostCodeController::class, 'index']);
Route::post('/postcodeform', [PostCodeController::class, 'postCodeForm']);
Route::post('/latlongform', [PostCodeController::class, 'latLongForm']);
