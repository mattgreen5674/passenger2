start:
	docker-compose up -d

stop:
	docker-compose stop

build:
	docker-compose rm -vsf
	docker-compose down -v --remove-orphans
	docker-compose build
	docker-compose up -d
	docker-compose exec -w '/application' php-fpm composer i

seed:
	docker-compose exec -w '/application' php-fpm php artisan migrate:fresh --seed
	docker-compose exec -w '/application' php-fpm php artisan storage:link

app-bash:
	docker exec -it passenger-php-fpm /bin/bash

import:
	docker-compose exec -w '/application' php-fpm php artisan import:postcode --fileName=$(fileName)
	