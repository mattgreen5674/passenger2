<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PostCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post_codes')->insert([
            'post_code' => 'AB1 0DD',
            'searchable' => 'ab10dd',
            'latitude' => 57.097810,
            'longitude' => -2.228006,
            'created_at' => Carbon::now()
        ]);
    }
}
