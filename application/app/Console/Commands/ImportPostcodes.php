<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use App\Models\PostCode;
use Carbon\Carbon;

class ImportPostcodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:postcodes {--fileName=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Post Code data into the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle() : void
    {
        $filePath = 'public/uploads/';
        $fileName = ($this->option('fileName')) ? $this->option('fileName') : 'test.csv';
        $filePathName = $filePath . $fileName;
        
        $this->info("Started: \n$this->description");

        if (!file_exists("$filePathName")) {
            $this->error("The file $fileName does not exist in the directory $filePath");
            return;
        }

        $importFile = fopen("$filePath$fileName", 'r') 
            or die("The file $fileName could not be opened"); 

        $headings = fgetcsv($importFile);

        $totalCount = 0;
        $savedCount = 0;
        $errorCount = 0;
        $validationErrorCount = 0;

        try {
            while (!feof($importFile)) {
                $row = fgetcsv($importFile);
                $mappedRow = $this->mapPostCodeData(array_combine($headings, $row));
                $validatedRow = $this->validatePostCodeData($mappedRow);
                
                if ($validatedRow) {
                    if ($this->savePostCode($mappedRow)) {
                        $savedCount++;
                    } else {
                        $errorCount++;
                        $this->logError($mappedRow, 'There was a problem saving the record.');
                    }
                } else {
                    $validationError++;
                }
    
                $totalCount++;
            }
    
            fclose($importFile);
    
            $this->info("Finished: \n$this->description");
            $this->line("Total rows processed: $totalCount");
            $this->info("Successfully saved imports: $savedCount");
            $this->error("Failed to save imports: $errorCount");
            $this->warn("Failed validation imports: $validationErrorCount");

        } catch (\Illuminate\Database\QueryException $e) {

            $this->error($e->getMessage());
            fclose($importFile);

        } catch (Exception $e) {
            $this->error($e->getMessage());
            fclose($importFile);
        }
    }

    private function mapPostCodeData(array $row) : array
    {
        $mappedRow = [];

        if (!empty($row)) {
            $mappedRow['post_code'] = $row['pcd'];
            $mappedRow['latitude'] = $row['lat'];
            $mappedRow['longitude'] = $row['long'];
        }

        return $mappedRow;
    }

    private function validatePostCodeData(array $row) : bool
    {
        $validator = Validator::make(
            $row,
            PostCode::getRules()
        );
        
        if ($validator->fails()) {
            $this->info("Post code {$row['post_code']} failed validation: ");
            foreach ($validator->errors()->all() as $error) {
                $this->logError($row, $error);
                $this->error($error);
            }
            return false;
        }

        return true;
    }

    private function savePostCode(array $row) : bool
    {
        $postCode = PostCode::where('post_code', '=', $row['post_code'])->firstOrFail();
        
        if(empty($postCode)){
            $postCode = new PostCode();
        } else {
            $postCode->updated_at = Carbon::now();
        }
        
        $postCode->post_code = $row['post_code'];
        $postCode->latitude = $row['latitude'];
        $postCode->longitude =$row['longitude'];
        $postCode->searchable = strtolower(str_replace(' ', '', $row['post_code']));
        
        return $postCode->save();
    }

    private function logError(array $row, string $errorMessage) : void
    {
        // Log the error!!
    }
}
